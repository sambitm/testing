class HomeController < ApplicationController
  def index
  end

  def fetch_events
    req = URI.encode("https://rest.bandsintown.com/artists/#{params[:artist]}/events?app_id=AdmitMe&date=#{params[:event_date]}")
    response = HTTParty.get(req)
    @event_urls = response.parsed_response.map {|resp| resp['url']}.join(', ')
    render status: :ok
  end
end

