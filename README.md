# README

## Set up the application

* Clone the project into local machine
* install the bundler (bundle install)
* rake db:migrate
* rake db:seed
* Start the development server (rails s)

Now got to localhost:3000 to access the application.
